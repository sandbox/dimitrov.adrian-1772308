CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Downloads
 * Installation
 * Contacts


INTRODUCTION
------------

View current page's watchdog messages directly in your browser's console.
Using watchdog hook, the module proccess and output the messages using,
console.log() JavaScript function.

What is Watchdog
http://api.drupal.org/api/drupal/includes%21bootstrap.inc/function/watchdog/7

FEATURES
--------
 * Set different filters for types, also supports and wildcards (* and ?)
 * Customize the message using tokens.
 

REQUIREMENTS
------------

 * Drupal 7.x


DOWNLOAD
-----------

Project page: http://drupal.org/node/1772308
Git (drupal.org): http://git.drupal.org/sandbox/dimitrov.adrian/1772308.git


INSTALLATION
------------

 1) Unarchive the zip and put directory into your sites/all/modules
 2) Enable it from your admin panel -> Modules


CONTACTS
--------
Developer: Adrian Dimtrov <adimitrov@propeople.dk>
Company: ProPeople (www.wearepropeople.com)
