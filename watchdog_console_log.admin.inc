<?php

/**
 * @file
 * The admin page for the watchdog console log module.
 */


/**
 * Watchdog Console Log configuration form.
 *
 * @param array $form
 *   render array
 * @param array $form_state
 *   form states
 *
 * @return array
 *   render array
 */
function watchdog_console_log_configuration_form($form, &$form_state) {

  $filters = variable_get('watchdog_console_log_get_filters', array('*'));

  // Create placeholder for holding the filters.
  $form['filters'] = array(
    '#title' => t('Filters'),
    '#description' => t('Coma-separated list of types (allowed wildcards * and ?). If you want to remove a filter, just empty it&rsquo;s field.'),
    '#type' => 'fieldset',
    '#attributes' => array('id' => 'watchdog-console-log-filters'),
  );

  // There is two cases, if we are manipulated already the form,
  // or just initing it.
  $i = 0;
  if (isset($form_state['values']['filters'])) {
    // Read the fields from the cached form in the session.
    $filters = array();
    foreach ($form_state['values']['filters'] as $key => $filter) {
      $form['filters'][$key] = watchdog_console_log_filters_element_render($key, $filter);
      $i++;
    }
  }
  else {
    // Initialize the form from the variable.
    foreach ($filters as $key => $filter) {
      $form['filters'][$i] = watchdog_console_log_filters_element_render($i, $filter);
      $i++;
    }
  }

  // Append newly added fields.
  $delta = empty($form_state['filters']) ? 0 : count($form_state['filters']);
  if (!($delta + $i)) {
    drupal_set_message(t('Currently there is no added filters. No messages will be outputed to your console log.'), 'warning');
  }
  $form['filters'][$i] = watchdog_console_log_filters_element_render($i, '');

  // Make form fancy.
  $form['filters-controls'] = array(
    '#type' => 'fieldset',
    'add_filter' => array(
      '#type' => 'button',
      '#value' => t('Add Filter'),
      '#ajax' => array(
        'callback' => 'watchdog_console_log_configuration_form_ajax_add',
        'wrapper' => 'watchdog-console-log-filters',
      ),
    ),
  );

  // Create placeholder for holding the groups.
  $form['message_format'] = array(
    '#title' => t('Message format'),
    '#type' => 'fieldset',
    '#type' => 'textfield',
    '#default_value' => variable_get('watchdog_console_log_message_format', 'Drupal.watchdog - %message'),
    '#description' => t('Available tokens: :tokens', array(':tokens' => '%type, %message, %severity, %link, %user, %uid, %request_uri, %referer, %ip, %timestamp')),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }

  return $form;
}

/**
 * Render filter form element.
 *
 * @param int $delta
 *   the current number
 * @param string $filter
 *   text filter
 *
 * @return array
 *   render array
 */
function watchdog_console_log_filters_element_render($delta = 0, $filter = '') {
  return array(
    '#type' => 'textfield',
    '#tree' => TRUE,
    '#parents' => array('filters', $delta),
    '#default_value' => $filter,
  );
}

/**
 * Implements FORM_ID_submit().
 */
function watchdog_console_log_configuration_form_submit($form, $form_state) {
  variable_set('watchdog_console_log_message_format', $form_state['values']['message_format']);
  variable_set('watchdog_console_log_get_filters', array_filter($form_state['values']['filters']));
}

/**
 * AJAX callback for adding new section.
 *
 * @param array $form
 *   render array
 * @param array $form_state
 *   form states
 *
 * @return array
 *   render array
 */
function watchdog_console_log_configuration_form_ajax_add($form, &$form_state) {
  return $form['filters'];
}

/**
 * AJAX callback for removing section.
 *
 * @param array $form
 *   render array
 * @param array $form_state
 *   form states
 *
 * @return array
 *   render array
 */
function watchdog_console_log_configuration_form_ajax_remove($form, $form_state) {
  return '';
}
